function updateCurrentTime() {
  $.ajax({
    url: "https://worldtimeapi.org/api/ip",
    method: "GET",
    success: function (response) {
      const currentTime = response.datetime;
      $("#currentTime").text(currentTime);
    },
    error: function (error) {
      console.error("Error fetching time:", error);
    },
  });
}

updateCurrentTime();
setInterval(updateCurrentTime, 1000);
